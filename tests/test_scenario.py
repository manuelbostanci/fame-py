# -*- coding:utf-8 -*-
from typing import List

import pytest

from fameio.source.scenario import Scenario, ScenarioException, General, Agent, Attribute, Contract
from tests.utils import assert_exception_contains, new_agent, new_schema


def new_scenario(schema: dict = None, general: dict = None, agents: list = None, contracts: list = None) -> dict:
    """Creates a new scenario definition based on given `schema`, `general` dicts plus `agents` and `contract lists`"""
    scenario = dict()
    if schema:
        # noinspection PyProtectedMember
        scenario[Scenario._KEY_SCHEMA] = schema
    if general:
        # noinspection PyProtectedMember
        scenario[Scenario._KEY_GENERAL] = general
    if agents:
        # noinspection PyProtectedMember
        scenario[Scenario._KEY_AGENTS] = agents
    if contracts:
        # noinspection PyProtectedMember
        scenario[Scenario._KEY_CONTRACTS] = contracts
    return scenario


class TestScenario:
    schema = new_schema([new_agent("Agent", [], [])])
    general = {'RunId': 1, 'Simulation': {'StartTime': 0, 'StopTime': 0}}

    def test_init_missing_schema(self):
        with pytest.raises(ScenarioException) as e_info:
            Scenario(new_scenario(None, dict(), list(), list()))
        assert_exception_contains(Scenario._MISSING_KEY, e_info)

    def test_init_missing_general(self):
        with pytest.raises(ScenarioException) as e_info:
            Scenario(new_scenario(TestScenario.schema, None, list(), list()))
        assert_exception_contains(Scenario._MISSING_KEY, e_info)

    def test_init_missing_agents(self):
        with pytest.raises(ScenarioException) as e_info:
            Scenario(new_scenario(TestScenario.schema, TestScenario.general, None, list()))
        assert_exception_contains(Scenario._MISSING_KEY, e_info)

    def test_init_missing_contracts(self):
        with pytest.raises(ScenarioException) as e_info:
            Scenario(new_scenario(TestScenario.schema, TestScenario.general, list(), None))
        assert_exception_contains(Scenario._MISSING_KEY, e_info)

    def test_init_agent_id_not_unique(self):
        agents = [{'Type': 'Agent', 'Id': 9}, {'Type': 'Agent', 'Id': 8}, {'Type': 'Agent', 'Id': 9}]
        with pytest.raises(ScenarioException) as e_info:
            Scenario(new_scenario(TestScenario.schema, TestScenario.general, agents, list()))
        assert_exception_contains(Scenario._AGENT_ID_NOT_UNIQUE, e_info)

    def test_init_agent_type_unknown(self):
        agents = [{'Type': 'Agent', 'Id': 9}, {'Type': 'Unknown', 'Id': 4}]
        with pytest.raises(ScenarioException) as e_info:
            Scenario(new_scenario(TestScenario.schema, TestScenario.general, agents, list()))
        assert_exception_contains(Scenario._AGENT_TYPE_UNKNOWN, e_info)


class TestGeneral:
    def test_init_missing_run_id(self):
        general = General({'Simulation': {'StartTime': 0, 'StopTime': 0}})
        assert general.run_id == 1

    def test_init_with_run_id(self):
        general = General({'RunId': 66, 'Simulation': {'StartTime': 0, 'StopTime': 0}})
        assert general.run_id == 66

    def test_init_missing_simulation(self):
        with pytest.raises(ScenarioException) as e_info:
            General({'RunId': 66})
        assert_exception_contains(General._MISSING_KEY, e_info)

    def test_init_missing_start_time(self):
        with pytest.raises(ScenarioException) as e_info:
            General({'RunId': 66, 'Simulation': {'StopTime': 0}})
        assert_exception_contains(General._MISSING_KEY, e_info)

    def test_init_missing_stop_time(self):
        with pytest.raises(ScenarioException) as e_info:
            General({'RunId': 66, 'Simulation': {'StartTime': 0}})
        assert_exception_contains(General._MISSING_KEY, e_info)

    def test_init_missing_random_seed(self):
        general = General({'Simulation': {'StartTime': 0, 'StopTime': 0}})
        assert general.random_seed == 1

    def test_init_with_random_seed(self):
        general = General({'Simulation': {'StartTime': 0, 'StopTime': 0, 'RandomSeed': 87}})
        assert general.random_seed == 87

    def test_init_missing_output(self):
        general = General({'Simulation': {'StartTime': 0, 'StopTime': 0}})
        assert general.output_process == 0
        assert general.output_interval == 100

    def test_init_with_output(self):
        general = General({'Simulation': {'StartTime': 0, 'StopTime': 0}, 'Output': {'Process': 8, 'Interval': 77}})
        assert general.output_process == 8
        assert general.output_interval == 77


class TestAgent:
    def test_init_missing_id(self):
        with pytest.raises(ScenarioException) as e_info:
            Agent({'Type': 'AgentType'})
        assert_exception_contains(Agent._MISSING_KEY, e_info)

    def test_init_missing_type(self):
        with pytest.raises(ScenarioException) as e_info:
            Agent({'Id': 99})
        assert_exception_contains(Agent._MISSING_KEY, e_info)


class TestAttribute:
    def test_init_missing_value(self):
        with pytest.raises(ScenarioException) as e_info:
            Attribute("AnAttribute", None)
        assert_exception_contains(Attribute._VALUE_MISSING, e_info)

    def test_init_zero_not_missing_value(self):
        assert Attribute("AnAttribute", 0) is not None

    def test_init_single_value(self):
        value = 5
        attribute = Attribute("MyAttribute", value)
        assert attribute.has_nested() is False
        assert attribute.value == value

    def test_init_list(self):
        value = [5.0, 7.0, 9.9]
        attribute = Attribute("MyAttribute", value)
        assert attribute.has_nested() is False
        assert attribute.value == value

    def test_init_nested_fail_missing_value(self):
        with pytest.raises(ScenarioException) as e_info:
            Attribute("AnAttribute", {"InnerAttrib": None})
        assert_exception_contains(Attribute._VALUE_MISSING, e_info)

    def test_init_nested(self):
        value = "MyInnerValue"
        attribute = Attribute("AnAttribute", {"InnerAttrib": value})
        assert attribute.has_nested() is True
        assert attribute.value is None
        inner_attribute = attribute.get_nested_by_name("InnerAttrib")
        assert inner_attribute.value == value

    def test_init_empty_list(self):
        with pytest.raises(ScenarioException) as e_info:
            Attribute("AnAttribute", [])
        assert_exception_contains(Attribute._LIST_EMPTY, e_info)

    def test_init_empty_dict(self):
        with pytest.raises(ScenarioException) as e_info:
            Attribute("AnAttribute", {})
        assert_exception_contains(Attribute._DICT_EMPTY, e_info)

    def test_init_nested_list(self):
        attribute = Attribute("AnAttribute", [{"A": 1, "B": 2}, {"A": 10, "B": 20}])
        assert attribute.has_nested_list()
        assert not attribute.has_nested()
        assert not attribute.has_value()
        nested_list = attribute.get_nested_list()
        assert len(nested_list) == 2
        assert nested_list[0]["A"].value == 1
        assert nested_list[1]["A"].value == 10

    def test_init_nested_list_inconsistent_data(self):
        with pytest.raises(ScenarioException) as e_info:
            Attribute("AnAttribute", [{"A": 1, "B": 2}, 5])
        assert_exception_contains(Attribute._MIXED_DATA, e_info)


class TestContract:
    def test_init_missing_sender(self):
        with pytest.raises(ScenarioException) as e_info:
            Contract({'ReceiverId': 1, 'ProductName': 'ProdA', 'FirstDeliveryTime': 0, 'DeliveryIntervalInSteps': 1})
        assert_exception_contains(Contract._MISSING_KEY, e_info)

    def test_init_missing_receiver(self):
        with pytest.raises(ScenarioException) as e_info:
            Contract({'SenderId': 0, 'ProductName': 'ProdA', 'FirstDeliveryTime': 0, 'DeliveryIntervalInSteps': 1})
        assert_exception_contains(Contract._MISSING_KEY, e_info)

    def test_init_missing_product(self):
        with pytest.raises(ScenarioException) as e_info:
            Contract({'SenderId': 0, 'ReceiverId': 1, 'FirstDeliveryTime': 0, 'DeliveryIntervalInSteps': 1})
        assert_exception_contains(Contract._MISSING_KEY, e_info)

    def test_init_missing_delivery_time(self):
        with pytest.raises(ScenarioException) as e_info:
            Contract({'SenderId': 0, 'ReceiverId': 1, 'ProductName': 'ProdA', 'DeliveryIntervalInSteps': 1})
        assert_exception_contains(Contract._MISSING_KEY, e_info)

    def test_init_missing_delivery_interval(self):
        with pytest.raises(ScenarioException) as e_info:
            Contract({'SenderId': 0, 'ReceiverId': 1, 'ProductName': 'ProdA', 'FirstDeliveryTime': 0})
        assert_exception_contains(Contract._MISSING_KEY, e_info)

    def test_init_missing_expiration_time(self):
        contract = Contract({'SenderId': 0, 'ReceiverId': 1, 'ProductName': 'ProdA', 'FirstDeliveryTime': 0,
                             'DeliveryIntervalInSteps': 1})
        assert contract.expiration_time is None

    def test_init_with_expiration_time(self):
        contract = Contract({'SenderId': 0, 'ReceiverId': 1, 'ProductName': 'ProdA', 'FirstDeliveryTime': 0,
                             'DeliveryIntervalInSteps': 1, 'ExpirationTime': 2000})
        assert contract.expiration_time == 2000

    def test_split_contract_definitions_one_to_one(self):
        definition = {'SenderId': 0, 'ReceiverId': 1, 'ProductName': 'ProdA', 'FirstDeliveryTime': 0,
                      'DeliveryIntervalInSteps': 1}
        result = Contract.split_contract_definitions(definition)
        assert len(result) == 1

    def test_split_contract_definitions_missing_sender(self):
        definition = {'ReceiverId': 1, 'ProductName': 'ProdA', 'FirstDeliveryTime': 0, 'DeliveryIntervalInSteps': 1}
        with pytest.raises(ScenarioException) as e_info:
            Contract.split_contract_definitions(definition)
        assert_exception_contains(Contract._MISSING_KEY, e_info)

    def test_split_contract_definitions_missing_receiver(self):
        definition = {'SenderId': 0, 'ProductName': 'ProdA', 'FirstDeliveryTime': 0, 'DeliveryIntervalInSteps': 1}
        with pytest.raises(ScenarioException) as e_info:
            Contract.split_contract_definitions(definition)
        assert_exception_contains(Contract._MISSING_KEY, e_info)

    def test_split_contract_definitions_m_to_n(self):
        definition = {'SenderId': [0, 1, 2], 'ReceiverId': [1, 2, 3, 4], 'ProductName': 'ProdA', 'FirstDeliveryTime': 0,
                      'DeliveryIntervalInSteps': 1}
        with pytest.raises(ScenarioException) as e_info:
            Contract.split_contract_definitions(definition)
        assert_exception_contains(Contract._MULTI_CONTRACT_CORRUPT, e_info)

    def test_split_contract_definitions_one_to_n(self):
        definition = {'SenderId': [0, 1, 2], 'ReceiverId': 5, 'ProductName': 'ProdA', 'FirstDeliveryTime': 0,
                      'DeliveryIntervalInSteps': 1}
        result = Contract.split_contract_definitions(definition)
        assert len(result) == 3
        TestContract.assert_contract_dicts_match(result)
        TestContract.assert_contract_x_to_y([0, 1, 2], [5, 5, 5], result)

    @staticmethod
    def assert_contract_dicts_match(contracts: List[dict]) -> None:
        """Asserts that for given list of `contracts`, all values except for keys 'senderId' & 'receiverId' match"""
        for index in range(1, len(contracts)):
            for key in [Contract._KEY_PRODUCT, Contract._KEY_FIRST_DELIVERY, Contract._KEY_FIRST_DELIVERY,
                        Contract._KEY_INTERVAL, Contract._KEY_EXPIRE, Contract._KEY_ATTRIBUTES]:
                if key in contracts[0]:
                    assert contracts[index][key] == contracts[0][key]

    @staticmethod
    def assert_contract_x_to_y(senders: List[int], receivers: List[int], contracts: List[dict]) -> None:
        """"Asserts that the n-th item in `contracts` matches the n-th of `senders` and `receivers`"""
        for index in range(len(contracts)):
            assert contracts[index][Contract._KEY_SENDER] == senders[index]
            assert contracts[index][Contract._KEY_RECEIVER] == receivers[index]

    def test_split_contract_definitions_n_to_one(self):
        definition = {'SenderId': 0, 'ReceiverId': [5, 6, 7], 'ProductName': 'ProdA', 'FirstDeliveryTime': 0,
                      'DeliveryIntervalInSteps': 1}
        result = Contract.split_contract_definitions(definition)
        assert len(result) == 3
        TestContract.assert_contract_dicts_match(result)
        TestContract.assert_contract_x_to_y([0, 0, 0], [5, 6, 7], result)

    def test_split_contract_definitions_n_to_n(self):
        definition = {'SenderId': [1, 2, 3], 'ReceiverId': [5, 6, 7], 'ProductName': 'ProdA', 'FirstDeliveryTime': 0,
                      'DeliveryIntervalInSteps': 1}
        result = Contract.split_contract_definitions(definition)
        assert len(result) == 3
        TestContract.assert_contract_dicts_match(result)
        TestContract.assert_contract_x_to_y([1, 2, 3], [5, 6, 7], result)
