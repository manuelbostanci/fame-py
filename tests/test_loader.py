# -*- coding:utf-8 -*-

from fameio.source.loader import load_yaml


class Test:
    def test_load_yaml_plain(self):
        expected = {"ToBe": "ThatIs", "OrNot": "TheQuestion"}
        loaded = load_yaml("tests/yaml/simple.yaml")
        assert expected == loaded

    def test_load_yaml_with_simple_include(self):
        expected = {"Is": {"ToBe": "ThatIs", "OrNot": "TheQuestion"}}
        loaded = load_yaml("tests/yaml/simple_include.yaml")
        assert expected == loaded

    def test_load_yaml_with_nested_include(self):
        expected = {"ToBe": {"ThatIs": {"Or": "maybe"}, "TheQuestion": {"not": "?"}}, "OrNot": {"not": "?"}}
        loaded = load_yaml("tests/yaml/a.yaml")
        assert expected == loaded
