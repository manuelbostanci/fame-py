# -*- coding:utf-8 -*-

import pytest

from fameio.source.schema import SchemaException, Schema
from tests.utils import assert_exception_contains, new_agent, new_schema


class Test:
    def test_init_empty(self):
        with pytest.raises(SchemaException) as e_info:
            Schema({})
        assert_exception_contains(Schema._AGENT_TYPES_MISSING, e_info)

    def test_init_key_word_mixed_caps(self):
        assert Schema({'AgEntTYPeS': {}})

    def test_init_empty_agent_types(self):
        assert Schema(new_schema([]))

    def test_init_adds_agent(self):
        schema = Schema(new_schema([new_agent("EmptyAgent", [], [])]))
        assert "EmptyAgent" in schema.types.keys()
