# Next release - vX.X (TBA.)
## minor changes
* Added: documentation on installation using `pipx` 
* Added: optional argument `-se`/`--singleexport` for exporting individual files for each agent


# [Fomalhaut - v1.5 (2021-06-30)](https://gitlab.com/fame-framework/fame-io/-/tags/v1.5)
## new feature
* huge speed up of result conversion due to update to latest protobuf package and refactoring 

## minor changes
* command line interface slightly enhanced: convert_results now supports specifying an output folder name

# [Etamin - v1.4 (2021-06-10)](https://gitlab.com/fame-framework/fame-io/-/tags/v1.4)
## breaking changes
* Minimum requirement is `fameprotobuf==1.1.4`

## new features:
* Schema: Attributes may specify "Default" values - these are used in case a mandatory attribute is not specified in the Scenario
* Attributes can now be "List": If so, multiple values can be assigned
* New AttributeTypes "Long", "String" and "TimeStamp" available
* Improved validations for Schema and Scenario
* Compact definition of multiple contracts

## minor changes
* Changed: major refactoring of make_config.py: split into several classes and packages, improved exception handling
* Changed: switched to pytest and improved test coverage
* Keywords in Schema and Scenario are no longer case-sensitive
* Minor bugs fixed

# [Deneb - v1.3 (2021-04-13)](https://gitlab.com/fame-framework/fame-io/-/tags/v1.3)
## breaking changes:
* input and output protobuf files use new format `DataStorage` allowing `FAME-Core` input and output to be written to the same file (requires `FAME-Core > 1.0`)
* `Attributes` in agents (formerly known as `Fields`) can be structured and allow complex tree-like data dictionaries
* contracts can also be equipped with `Attributes` supporting `int`, `float`, `enum` or `dict` data structures
* automatic detection of `TimeStamps` by string format and conversion to int64

## minor changes:
* updated to `fameprotobuf==1.1.2`
* `log_and_raise` error when file can not be loaded triggered by `!include` command
* critical error is raised when trying to convert empty protobuf output file
* check if `product` in `contract` is valid according to `schema.yaml`
* added coverage report badge
* added `CHANGELOG.md`

# [Cebalrai - v1.2.4 (2021-02-26)](https://gitlab.com/fame-framework/fame-io/-/tags/v1.2.4)
## breaking changes:
None

## minor changes:
* moved `is_compatible` function to class `AttributeType`

# [Castor - v1.2.3 (2021-02-24)](https://gitlab.com/fame-framework/fame-io/-/tags/v1.2.3)
## breaking changes:
None

## minor changes:
* bug fix in file prefix `IGNORE_` (used when loading a set of contract files with the !include argument) is now working consistently

# [Caph - v1.2.2 (2021-02-18)](https://gitlab.com/fame-framework/fame-io/-/tags/v1.2.2)
## breaking changes:
* renamed `fieldtype` to `attributetype` in `schema.yaml`

## minor changes:
* protobuffer imports are now derived from `fameprotobuf` package
* improved handling of cases for keys in `scenario.yaml`
* improved handling of time stamp strings

# [Capella - v1.2.1 (2021-02-10)](https://gitlab.com/fame-framework/fame-io/-/tags/v1.2.1)
## breaking changes:
None

## minor changes:
* Improved key handling for contracts which are now case-insensitive

# [Canopus - v1.2 (2021-02-04)](https://gitlab.com/fame-framework/fame-io/-/tags/v1.2)
## breaking changes:
* renamed package to `fameio`

## minor changes:
* implemented improved yaml loader which allows integrating additional yaml files by the command `!include "path/to/external/file.yaml"`
* improved executables
* restructured logging
* bug fixes
* improved documentation

# [Bellatrix - v1.1 (2020-12-09)](https://gitlab.com/fame-framework/fame-io/-/tags/v1.1)
## breaking changes:
None

## minor changes:
* completed PyPI packaging
* implemented executables for calling `makeFameRunConfig` and `convertFameResults`
* improved documentation

# [Altair - v1.0 (2020-11-17)](https://gitlab.com/fame-framework/fame-io/-/tags/v1.0)
Initial release of `famepy`
