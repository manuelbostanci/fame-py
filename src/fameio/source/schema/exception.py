# -*- coding:utf-8 -*-

class SchemaException(Exception):
    """An exception that occurred while parsing a Schema"""
    pass
